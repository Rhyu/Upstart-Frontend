import React from 'react';

import UpstartService from "../service/UpstartService";

export default class UpstartEntities extends React.Component {
    state = {
        entities: []
    };

    async componentDidMount() {
        const entities = await UpstartService.getAllEntities();
        this.setState({entities});
    }

    render() {
        return (
            <ul>
                {this.state.entities.map(entity => <li>{entity.id} || {entity.data}</li>)}
            </ul>
        )
    }
}