import axios from "axios";

const instance = axios.create({
    baseURL: 'http://localhost:8080/api/',
    timeout: 2000,
});

const UpstartService = {
    getEntityById: function (id) {
    },

    getAllEntities: function () {
        return instance.get("upstart/entity/all").then(res => {
            if (res.data.status === "SUCCESS") {
                console.log(res.data.entities);
                return res.data.entities;
            } else {
                console.log("Error on upstart/entity/all");
                return null;
            }
        })
    },

    addEntity: function (entity) {
    },

    deleteEntity: function (entity) {
    },
};

export default UpstartService;