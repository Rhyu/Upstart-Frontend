import React from 'react';
import './App.css';
import UpstartEntities from "./component/UpstartEntities";

function App() {
    return (
        <div className="App">
            <UpstartEntities/>
        </div>
    );
}

export default App;
